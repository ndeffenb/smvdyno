function setRegister(device, registerAddress, mask)
% Sets specified register on the specified I2C device using the specified
% mask. All binary 1 will be set (if not set already) and all zeroes will
% remain unchanged. It accomplishes this by first reading the existing
% contents from the register, doing a bitwise OR with the mask, then
% writing the register with the result.

    valueRead=readRegister(device, registerAddress); %get existing contents
    valueWrite=bitor(valueRead, bin2dec(mask)); %apply mask
    writeRegister(device, registerAddress, valueWrite); %set register
end