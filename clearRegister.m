function clearRegister(device, registerAddress, mask)
% Clears specified register on the specified I2C device using the specified
% mask. All binary 0 will be cleared (if not set already) and all 1s will
% remain unchanged. It accomplishes this by first reading the existing
% contents from the register, doing a bitwise AND with the mask, then
% writing the register with the result.
    valueRead=readRegister(device, registerAddress); %get existing contents
    valueWrite=bitand(valueRead, bin2dec(mask)); %apply mask
    writeRegister(device, registerAddress, valueWrite); %set register
end