# README #

This repository features the code necessary to run and develop the SMVDyno.

### What is this repository for? ###

* Non-developing users
* Developers looking to fork

### How do I get set up? ###

* Non-developing users: Download a .zip file and place it in your MATLAB repository. 
* Developing users: Fork the repository and use your preferred GIT client to acquire.

### Where do I get help? ###

* SMVDyno FDR
* SMVDyno Software User's Manual