function loadCellSetup(device)
% sets up the load cell.

    %reset all registers to power-off defaults. This requries setting bit 0 of
    %the 0 register, waiting a split second, then clearing it.
    setRegister(device, 0, '00000001');
    pause(0.01);
    clearRegister(device, 0, '11111110');
    %power up the digital circuit by setting bit 1 of register 0
    setRegister(device, 0, '00000010');
    %spower up the analog circuit by setting bit 2 of register 0. This requries
    %bit 1 of register 0 to be set.
    setRegister(device, 0, '00000100');
    %check the read-only bit 3 of register 0, which is set when the power-up is
    %ready. Do not proceed until it is set. Timeout otherwise.
    tic
    while (~any(bitand(readRegister(device, 0), bin2dec('00001000'))))
        if(toc>1)
            %timeout
            error('ADC would not power on');
        end
    end
    disp('ADC powered on successfully');
    %Manipulate register 1. Set the LDO voltage to 3.3v. The LDO setting takes
    %up bits 3 through5 in register 1. Set them to 100 for 3.3v.
    clearRegister(device, 1, '11100111');
    setRegister(device, 1, '00100000');
    %enable the internal LDO. Do this by setting bit 7 in register 0.
    setRegister(device, 0, '10000000');
    %Set the gain to x128. The gain takes up the next three bits in register 1,
    %which are set to 0b111.
    setRegister(device, 1, '100111'); 
    %set the conversion rate. This is done in register 2. Bits 4 through 6
    %control the conversion rate. We set it to max (320SPS), or 0b111
    setRegister(device, 2, '01110000');
    %turn the CLK_CHP to off. This is done in register 21 (0x15). The bits 4
    %and 5 are set to 0b11.
    setRegister(device, 21, '00110000');
    %calibrate the chip. Start this process by setting bit 2 of register 2.
    setRegister(device, 2, '00000100');
    %Wait for the same bit to be cleared. If it doesn't clear, there was an
    %error calibrating.
    tic
    while (any(bitand(readRegister(device, 2), bin2dec('00000100'))))
        if(toc>1)
            %timeout
            error('ADC did not calibrate');
        end
    end
    disp('ADC calibrated successfully');
end