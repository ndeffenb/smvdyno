function [a, device, encoder]=boardSetup()
    % sets up board, returns I2C load cell device and encoder

    %check if the variable a both exists and if it is an 'arduino' object.
    %Clear the variable and create it as an arduino object if not.
    if ~(exist('a','var') && isa(a,'arduino'))
        if (exist('arduino','var'))
            clear(a)
        end
        a=arduino('COM7','ProMini328_5V','Libraries','I2C, RotaryEncoder');
    end
    %scan the I2C bus and connect to the first address found. Note that this
    %may have to be altered if another device is added to the I2C bus.
    I2Cbus=scanI2CBus(a);
    device=i2cdev(a,I2Cbus{1});
    %set up the quadrature encoder on the two interrupt pins: D2 and D3
    encoder=rotaryEncoder(a,'D2','D3',4);
    %set up fan PWM pin
    configurePin(a,'D5','PWM');
    %set up temperature analog in pin
    configurePin(a,'A1','AnalogInput');
    %set up estop pin
    configurePin(a,'D8','pullup');
end