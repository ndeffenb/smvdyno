function [success, reading]=getLoadCellReading(device)
% Read from the load cell if possible. Return a 0 in "success" if the
% conversion is not ready. Return the reading in "reading"

    %check the cycle ready bit to see if it set. If it is set then the
    %conversion is complete, so we can move forward with reading the ADC
    %result. The CR bit is bit 5 of register 0.
    if any(bitand(readRegister(device, 0), bin2dec('00010000')))
        reading=NaN;
        success=0;
    else
        %read the resulting data from the adc conversion result registers.
        %Because the ADC is 24 bits, this result spans three 8-bit
        %registers: 0x12 (bits 23 to 16), 0x13 (bits 15 to 8) and 0x14
        %(bits 7 to 0). We use option 2, "I2C single read" to get the data.
        %First we read register 0x12, then register 0x13, then register
        %0x14. Note these registers are 18, 19 and 20 in decimal.
        highByte=dec2bin(readRegister(device, 18),8);
        midByte=dec2bin(readRegister(device, 19),8);
        lowByte=dec2bin(readRegister(device, 20),8);
        %concatenate these readings. 
        rawReading=[highByte midByte lowByte];
        %the sign bit is bit 23. To extract this, shift the whole thing
        %left by 8 such that the sign bit is at the MSB of a 32-bit number.
        shiftedReading=bitshift(bin2dec([highByte midByte lowByte]),8);
        %cast to a signed value and then back to an unsigned value.
        % https://www.mathworks.com/matlabcentral/answers/39416-binary-to-signed-decimal-in-matlab
        castedReading=typecast(uint32(shiftedReading), 'int32');
        %finally shift back right by 8 to get the correct value.
        reading=bitshift(castedReading,-8);
        success=1;
    end
end
        